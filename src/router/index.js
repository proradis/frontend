import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () =>
      import(/* webpackChunkName: "Registros" */ "../views/Registro.vue")
  },
  {
    path: "/Pacientes",
    name: "Pacientes",
    component: () =>
      import(/* webpackChunkName: "Pacientes" */ "../views/Pacientes.vue")
  },
  {
    path: "/Vacinas",
    name: "Vacinas",
    component: () =>
      import(/* webpackChunkName: "Vacinas" */ "../views/Vacinas.vue")
  },
  {
    path: "/Registros",
    name: "Registros",
    component: () =>
      import(/* webpackChunkName: "Registros" */ "../views/Registro.vue")
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
