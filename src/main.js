import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import axios from "axios"

import VueMaterial from "../node_modules/vue-material"
import "vue-material/dist/vue-material.min.css"
// import 'vue-material/dist/theme/default.css'
import "vue-material/dist/theme/black-green-dark.css"

import VueStripeMenu from "../node_modules/vue-stripe-menu"
import "vue-stripe-menu/dist/vue-stripe-menu.css"

import VueTheMask from "vue-the-mask"

import moment from "moment"

Vue.filter('showDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
})

Vue.use(VueStripeMenu)
Vue.use(VueMaterial)
Vue.use(VueTheMask)

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$urlAPI = "https://localhost:44306/api/"


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app")