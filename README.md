# front

## Project setup
```
npm install
```

## correct bug in the vue-material code base
```
1 - Locate node_modules\vue-material\dist\vue-material.js

2 - Search for isInvalidValue

3 - swap out the code:
```

```
return this.$el.validity.badInput;
to:
return this.$el.validity ? this.$el.validity.badInput : false;
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
